<?php

namespace Drupal\country_state_field;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for city.
 */
class CityTranslationHandler extends ContentTranslationHandler {

}
