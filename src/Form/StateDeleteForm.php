<?php

namespace Drupal\country_state_field\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting State entities.
 *
 * @ingroup country_state_field
 */
class StateDeleteForm extends ContentEntityDeleteForm {


}
