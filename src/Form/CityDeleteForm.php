<?php

namespace Drupal\country_state_field\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting City entities.
 *
 * @ingroup country_state_field
 */
class CityDeleteForm extends ContentEntityDeleteForm {


}
