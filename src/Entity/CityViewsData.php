<?php

namespace Drupal\country_state_field\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for City entities.
 */
class CityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
