<?php

namespace Drupal\country_state_field\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for State entities.
 */
class StateViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
