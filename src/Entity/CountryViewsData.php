<?php

namespace Drupal\country_state_field\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Country entities.
 */
class CountryViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
